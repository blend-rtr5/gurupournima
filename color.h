#ifndef __colors
#define __colors

typedef struct
{
    float red;
    float green;
    float blue;
} COLOR;

COLOR RED = {1.0f, 0.0f, 0.0f};
COLOR CERTIFICATES_COLORS = {0.7f, 0.5f, 0.4f};
COLOR GREEN = {0.0f, 1.0f, 0.0f};
COLOR BLUE = {0.0f, 0.0f, 1.0f};
COLOR BLACK = {0.0f, 0.0f, 0.0f};
COLOR GREY = {0.5f, 0.5f, 0.5f};
COLOR DARK_GREY = {0.25f, 0.25f, 0.25f};
COLOR LIGHT_GREY = {0.75f, 0.75f, 0.75f};
COLOR WHITE = {1.0f, 1.0f, 1.0f};
COLOR BACKGROUND = {0.85f, 0.815f, 0.76f};

void setColor(COLOR color)
{
    glColor3f(color.red, color.green, color.blue);
}
#endif