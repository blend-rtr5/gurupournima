#ifndef __coputer_2d
#define __coputer_2d

#include <math.h>
#include "color.h"

typedef int BOOL;
#define TRUE 1
#define FALSE 0

float X_OFFSET = 0.0f;
float Y_OFFSET = 0.0f;
float SCALE = 1.0f;

#define BASE_COUNT 3

float bottom_line[BASE_COUNT][2][2] = {
    {{0.45f, -0.40f}, {-0.28f, -0.40f}},  // line 1 right most
    {{-0.30f, -0.40f}, {-0.38f, -0.40f}}, // line 2
    {{-0.40f, -0.40f}, {-0.45f, -0.40f}}  // line 3 left most

};

#define TABLE_COUNT 3
float table[TABLE_COUNT][4][3] = {
    {{0.4f, -0.4f, 0.0f}, {0.4f, -0.20f, 0.0f}, {0.3f, -0.20f, 0.0f}, {0.3f, -0.4f, 0.0f}},
    {{-0.15f, -0.4f, 0.0f}, {-0.15f, -0.20f, 0.0f}, {0.00f, -0.20f, 0.0f}, {0.00f, -0.4f, 0.0f}},
    {{-0.20f, -0.15f, 0.0f}, {-0.20f, -0.20f, 0.0f}, {0.45f, -0.20f, 0.0f}, {0.45f, -0.15f, 0.0f}},

};

#define TABLE_DRAWER 7
float table_drawer[TABLE_DRAWER][4][3] = {
    {{0.4f, -0.21f, 0.0f}, {0.4f, -0.20f, 0.0f}, {0.3f, -0.20f, 0.0f}, {0.3f, -0.21f, 0.0f}},
    {{-0.15f, -0.21f, 0.0f}, {-0.15f, -0.20f, 0.0f}, {0.00f, -0.20f, 0.0f}, {0.00f, -0.21f, 0.0f}},
    {{0.4f, -0.26f, 0.0f}, {0.4f, -0.25f, 0.0f}, {0.3f, -0.25f, 0.0f}, {0.3f, -0.26f, 0.0f}},
    {{-0.15f, -0.31f, 0.0f}, {-0.15f, -0.30f, 0.0f}, {0.00f, -0.30f, 0.0f}, {0.00f, -0.31f, 0.0f}},
    {{0.4f, -0.31f, 0.0f}, {0.4f, -0.30f, 0.0f}, {0.3f, -0.30f, 0.0f}, {0.3f, -0.31f, 0.0f}},
    {{0.4f, -0.39f, 0.0f}, {0.4f, -0.40f, 0.0f}, {0.3f, -0.40f, 0.0f}, {0.3f, -0.39f, 0.0f}},
    {{-0.15f, -0.39f, 0.0f}, {-0.15f, -0.40f, 0.0f}, {0.0f, -0.40f, 0.0f}, {0.0f, -0.39f, 0.0f}}};

#define MONITOR_BLOCKS 4
float monitor[MONITOR_BLOCKS][4][3] = {
    {{0.10f, -0.15f, 0.0f}, {0.10f, -0.14f, 0.0f}, {0.20f, -0.14f, 0.0f}, {0.20f, -0.15f, 0.0f}},
    {{0.13f, -0.1f, 0.0f}, {0.13f, -0.14f, 0.0f}, {0.17f, -0.14f, 0.0f}, {0.17f, -0.1f, 0.0f}},
    {{0.05f, -0.1f, 0.0f}, {0.05f, 0.10, 0.0f}, {0.25f, 0.10f, 0.0f}, {0.25f, -0.1f, 0.0f}},
    {{0.06f, -0.07f, 0.0f}, {0.06f, 0.09, 0.0f}, {0.24f, 0.09f, 0.0f}, {0.24f, -0.07f, 0.0f}}};

COLOR monitor_color[MONITOR_BLOCKS] = {BLACK, BLACK, BLACK, LIGHT_GREY};

#define CPU_BLOCKS 9
float cpu[CPU_BLOCKS][4][3] = {
    {{0.30f, -0.15f, 0.0f}, {0.30f, 0.1f, 0.0f}, {0.38f, 0.1f, 0.0f}, {0.38f, -0.15f, 0.0f}},
    {{0.31f, -0.01f, 0.0f}, {0.31f, 0.08f, 0.0f}, {0.37f, 0.08f, 0.0f}, {0.37f, -0.01f, 0.0f}},
    {{0.31f, -0.14f, 0.0f}, {0.31f, -0.13f, 0.0f}, {0.37f, -0.13f, 0.0f}, {0.37f, -0.14f, 0.0f}},
    {{0.31f, -0.12f, 0.0f}, {0.31f, -0.125f, 0.0f}, {0.37f, -0.125f, 0.0f}, {0.37f, -0.12f, 0.0f}},
    {{0.31f, -0.11f, 0.0f}, {0.31f, -0.115f, 0.0f}, {0.37f, -0.115f, 0.0f}, {0.37f, -0.11f, 0.0f}},
    {{0.31f, -0.10f, 0.0f}, {0.31f, -0.095f, 0.0f}, {0.37f, -0.095f, 0.0f}, {0.37f, -0.10f, 0.0f}},
    {{0.31f, 0.03f, 0.0f}, {0.31f, 0.04f, 0.0f}, {0.37f, 0.04f, 0.0f}, {0.37f, 0.03f, 0.0f}},
    {{0.31f, 0.02f, 0.0f}, {0.31f, 0.015f, 0.0f}, {0.37f, 0.015f, 0.0f}, {0.37f, 0.02f, 0.0f}},
    {{0.31f, 0.07f, 0.0f}, {0.31f, 0.065f, 0.0f}, {0.37f, 0.065f, 0.0f}, {0.37f, 0.07f, 0.0f}}};

COLOR cpu_color[CPU_BLOCKS] = {BLACK, GREY, GREY, DARK_GREY, DARK_GREY, DARK_GREY, DARK_GREY, DARK_GREY, DARK_GREY};

#define BOOKS 4
float books[BOOKS][4][3] = {
    {{-0.15f, 0.27f, 0.0f}, {-0.15f, 0.37f, 0.0f}, {-0.13f, 0.37f, 0.0f}, {-0.13f, 0.27f, 0.0f}},
    {{-0.12f, 0.27f, 0.0f}, {-0.12f, 0.37f, 0.0f}, {-0.10f, 0.37f, 0.0f}, {-0.10f, 0.27f, 0.0f}},
    {{-0.09f, 0.27f, 0.0f}, {-0.09f, 0.37f, 0.0f}, {-0.07f, 0.37f, 0.0f}, {-0.07f, 0.27f, 0.0f}},
    {{-0.06f, 0.27f, 0.0f}, {-0.06f, 0.37f, 0.0f}, {-0.04f, 0.37f, 0.0f}, {-0.04f, 0.27f, 0.0f}},
};

COLOR book_color[BOOKS] = {BLUE, GREEN, RED, BLACK};

#define BOOKS_STICKER 12
float book_stickers[BOOKS_STICKER][4][3] = {
    {{-0.15f, 0.31f, 0.0f}, {-0.15f, 0.32f, 0.0f}, {-0.13f, 0.32f, 0.0f}, {-0.13f, 0.31f, 0.0f}},
    {{-0.15f, 0.30f, 0.0f}, {-0.15f, 0.295f, 0.0f}, {-0.13f, 0.295f, 0.0f}, {-0.13f, 0.30f, 0.0f}},
    {{-0.145f, 0.35f, 0.0f}, {-0.145f, 0.33f, 0.0f}, {-0.135f, 0.33f, 0.0f}, {-0.135f, 0.35f, 0.0f}},

    {{-0.12f, 0.31f, 0.0f}, {-0.12f, 0.32f, 0.0f}, {-0.10f, 0.32f, 0.0f}, {-0.10f, 0.31f, 0.0f}},
    {{-0.12f, 0.30f, 0.0f}, {-0.12f, 0.295f, 0.0f}, {-0.10f, 0.295f, 0.0f}, {-0.10f, 0.30f, 0.0f}},
    {{-0.115f, 0.35f, 0.0f}, {-0.115f, 0.33f, 0.0f}, {-0.105f, 0.33f, 0.0f}, {-0.105f, 0.35f, 0.0f}},

    {{-0.09f, 0.31f, 0.0f}, {-0.09f, 0.32f, 0.0f}, {-0.07f, 0.32f, 0.0f}, {-0.07f, 0.31f, 0.0f}},
    {{-0.09f, 0.30f, 0.0f}, {-0.09f, 0.295f, 0.0f}, {-0.07f, 0.295f, 0.0f}, {-0.07f, 0.30f, 0.0f}},
    {{-0.085f, 0.35f, 0.0f}, {-0.085f, 0.33f, 0.0f}, {-0.075f, 0.33f, 0.0f}, {-0.075f, 0.35f, 0.0f}},

    {{-0.06f, 0.31f, 0.0f}, {-0.06f, 0.32f, 0.0f}, {-0.04f, 0.32f, 0.0f}, {-0.04f, 0.31f, 0.0f}},
    {{-0.06f, 0.30f, 0.0f}, {-0.06f, 0.295f, 0.0f}, {-0.04f, 0.295f, 0.0f}, {-0.04f, 0.30f, 0.0f}},
    {{-0.055f, 0.35f, 0.0f}, {-0.055f, 0.33f, 0.0f}, {-0.045f, 0.33f, 0.0f}, {-0.045f, 0.35f, 0.0f}},
};

COLOR book_sticker_color[BOOKS_STICKER] = {GREY, GREY, GREY, GREY, GREY, GREY, GREY, GREY, GREY, GREY, GREY, GREY};

#define BOOKS_SHELF 1
float book_shelf[BOOKS_SHELF][4][3] = {
    {{-0.2f, 0.27f, 0.0f}, {-0.2f, 0.25f, 0.0f}, {0.4f, 0.25f, 0.0f}, {0.4f, 0.27f, 0.0f}},
};

#define CERTIFICATES 2
float ceritficates[CERTIFICATES][4][3] = {
    {{-0.45f, 0.0f, 0.0f}, {-0.45f, 0.15f, 0.0f}, {-0.3f, 0.15f, 0.0f}, {-0.3f, 0.0f, 0.0f}},
    {{-0.445f, 0.005f, 0.0f}, {-0.445f, 0.145f, 0.0f}, {-0.305f, 0.145f, 0.0f}, {-0.305f, 0.005f, 0.0f}},

};
COLOR ceritficate_colors[CERTIFICATES] = {WHITE, LIGHT_GREY};

#define CERTIFICATE_TEXT_LINES_COUNT 14
float certificate_line[CERTIFICATE_TEXT_LINES_COUNT][2][2] = {
    // boarders for certificate
    {{-0.45f, 0.0f}, {-0.45f, 0.15f}},
    {{-0.45f, 0.15f}, {-0.3f, 0.15f}},
    {{-0.3f, 0.15f}, {-0.3f, 0.0f}},
    {{-0.3f, 0.0f}, {-0.45f, 0.0f}},
    // boarders for certificate
    {{-0.445f, 0.005f}, {-0.445f, 0.145f}},
    {{-0.445f, 0.145f}, {-0.305f, 0.145f}},
    {{-0.305f, 0.145f}, {-0.305f, 0.005f}},
    {{-0.305f, 0.005f}, {-0.445f, 0.005f}},
    // content line
    {{-0.43f, 0.13f}, {-0.33f, 0.13f}},
    {{-0.43f, 0.115f}, {-0.33f, 0.115f}},
    {{-0.43f, 0.100f}, {-0.35f, 0.100f}},
    {{-0.43f, 0.085f}, {-0.33f, 0.085f}},
    {{-0.43f, 0.070f}, {-0.35f, 0.070f}},
    {{-0.43f, 0.050f}, {-0.38f, 0.050f}},
};

void drawComputerLab()
{
    // variable declarations
    void drawLines(float points[][2][2], int line_count);
    void drawTable(float points[][4][3], int count);
    void drawDrawers(float points[][4][3], int count);
    void drawMonitor(float points[][4][3], int count);
    void drawCpu(float points[][4][3], int count);
    void drawBookShelf(float points[][4][3], int count);
    void drawBooks();
    void drawCertificate();
    void drawMonitor3d();

    // cdoe
    // draw basic quad
    glBegin(GL_QUADS);
    setColor(BACKGROUND);
    glVertex3f(0.5f, 0.5f, 0.0f);
    glVertex3f(0.5f, -0.5f, 0.0f);
    glVertex3f(-0.5f, -0.5f, 0.0f);
    glVertex3f(-0.5f, 0.5f, 0.0f);

    glEnd();
    drawLines(bottom_line, BASE_COUNT);
    drawTable(table, TABLE_COUNT);
    drawDrawers(table_drawer, TABLE_DRAWER);
    drawMonitor(monitor, MONITOR_BLOCKS);
    drawMonitor3d();
    drawCpu(cpu, CPU_BLOCKS);
    drawBookShelf(book_shelf, BOOKS_SHELF);
    drawBooks();
    drawCertificate();
}

void drawPoint(float x, float y, float z)
{
    glVertex3f(x * SCALE + X_OFFSET, y * SCALE + Y_OFFSET, z);
}

void drawMonitor3d()
{
    //  {{0.06f, -0.07f, 0.0f}, {0.06f, 0.09, 0.0f}, {0.24f, 0.09f, 0.0f}, {0.24f, -0.07f, 0.0f}}};
    // code
    // quad 1
    glBegin(GL_QUADS);
    setColor(GREY);
    drawPoint(0.11, -0.04, 0.0f);
    setColor(BLACK);
    drawPoint(0.19, -0.04, 0.0f);
    setColor(WHITE);
    drawPoint(0.19, 0.05, 0.0f);
    setColor(BLACK);
    drawPoint(0.11, 0.05, 0.0f);
    glEnd();
    // quad 2
    glBegin(GL_QUADS);
    setColor(GREEN);
    drawPoint(0.13, 0.07, 0.0f);
    setColor(GREEN);
    drawPoint(0.21, 0.07, 0.0f);
    setColor(BLACK);
    drawPoint(0.19, 0.05, 0.0f);
    setColor(BLACK);
    drawPoint(0.11, 0.05, 0.0f);
    glEnd();
    // quad 3
    glBegin(GL_QUADS);
    setColor(RED);
    drawPoint(0.21, -0.02, 0.0f);
    setColor(BLACK);
    drawPoint(0.19, -0.04, 0.0f);
    setColor(BLACK);
    drawPoint(0.19, 0.05, 0.0f);
    setColor(RED);
    drawPoint(0.21, 0.07, 0.0f);
    glEnd();
}

void drawLines(float points[][2][2], int line_count)
{
    for (int i = 0; i < line_count; i++)
    {
        glBegin(GL_LINES);
        glColor3f(0.25f, 0.25f, 0.25f);

        for (int j = 0; j < 2; j++)
        {
            glVertex2f(SCALE * points[i][j][0] + X_OFFSET, SCALE * points[i][j][1] + Y_OFFSET);
        }
        glEnd();
    }
}

void drawTable(float points[][4][3], int count)
{
    // declarations
    void drawQuads(float points[][4][3], int count);
    // code
    glColor3f(0.25f, 0.25f, 0.25f);
    drawQuads(points, count);
}

void drawDrawers(float points[][4][3], int count)
{
    // declarations
    void drawQuads(float points[][4][3], int count);
    // code
    glColor3f(0.0f, 0.0f, 0.0f);
    drawQuads(points, count);
}

void drawMonitor(float points[][4][3], int count)
{
    // declarations
    void drawQuadsWithColor(float points[][4][3], int count, COLOR color[]);
    // code
    drawQuadsWithColor(points, count, monitor_color);
}

void drawCpu(float points[][4][3], int count)
{
    // declarations
    void drawQuadsWithColor(float points[][4][3], int count, COLOR color[]);
    // code

    drawQuadsWithColor(points, count, cpu_color);
}

void drawBookShelf(float points[][4][3], int count)
{
    // declarations
    void drawQuads(float points[][4][3], int count);
    // code
    glColor3f(DARK_GREY.red, DARK_GREY.green, DARK_GREY.blue);
    drawQuads(points, count);
}

void drawCertificate()
{

    // function declarations
    void drawCertificateSymbol();

    // code
    void drawQuadsWithColor(float points[][4][3], int count, COLOR color[]);

    Y_OFFSET = Y_OFFSET - 0.1 * SCALE;
    drawQuadsWithColor(ceritficates, CERTIFICATES, ceritficate_colors);
    drawLines(certificate_line, CERTIFICATE_TEXT_LINES_COUNT);
    drawCertificateSymbol();
    X_OFFSET = X_OFFSET + 0.18 * SCALE;
    drawQuadsWithColor(ceritficates, CERTIFICATES, ceritficate_colors);
    drawLines(certificate_line, CERTIFICATE_TEXT_LINES_COUNT);
    drawCertificateSymbol();
    Y_OFFSET = Y_OFFSET + 0.18 * SCALE;
    drawQuadsWithColor(ceritficates, CERTIFICATES, ceritficate_colors);
    drawLines(certificate_line, CERTIFICATE_TEXT_LINES_COUNT);
    drawCertificateSymbol();
    X_OFFSET = X_OFFSET - 0.18 * SCALE;
    drawQuadsWithColor(ceritficates, CERTIFICATES, ceritficate_colors);
    drawLines(certificate_line, CERTIFICATE_TEXT_LINES_COUNT);
    drawCertificateSymbol();
    Y_OFFSET = Y_OFFSET - 0.28 * SCALE;
}

void drawBooks()
{
    // declarations
    void drawQuadsWithColor(float points[][4][3], int count, COLOR color[]);
    // code
    drawQuadsWithColor(books, BOOKS, book_color);
    drawQuadsWithColor(book_stickers, BOOKS_STICKER, book_sticker_color);

    // draw books with offset
    X_OFFSET = X_OFFSET + 0.12 * SCALE;
    drawQuadsWithColor(books, BOOKS, book_color);
    drawQuadsWithColor(book_stickers, BOOKS_STICKER, book_sticker_color);
    X_OFFSET = X_OFFSET + 0.12 * SCALE;
    drawQuadsWithColor(books, BOOKS, book_color);
    drawQuadsWithColor(book_stickers, BOOKS_STICKER, book_sticker_color);
    X_OFFSET = X_OFFSET + 0.12 * SCALE;
    drawQuadsWithColor(books, BOOKS, book_color);
    drawQuadsWithColor(book_stickers, BOOKS_STICKER, book_sticker_color);
    X_OFFSET = X_OFFSET - 0.36 * SCALE;
}

void drawQuadsWithColor(float points[][4][3], int count, COLOR color[])
{
    // code
    for (int i = 0; i < count; i++)
    {
        glBegin(GL_QUADS);
        glColor3f(color[i].red, color[i].green, color[i].blue);
        for (int j = 0; j < 4; j++)
        {
            glVertex3f(SCALE * points[i][j][0] + X_OFFSET, SCALE * points[i][j][1] + Y_OFFSET, points[i][j][2]);
        }
        glEnd();
    }
}

void drawQuads(float points[][4][3], int count)
{
    // code
    for (int i = 0; i < count; i++)
    {
        glBegin(GL_QUADS);
        for (int j = 0; j < 4; j++)
        {
            glVertex3f(SCALE * points[i][j][0] + X_OFFSET, SCALE * points[i][j][1] + Y_OFFSET, points[i][j][2]);
        }
        glEnd();
    }
}

void drawCertificateSymbol()
{
    // function declarations
    void drawCircle(float radius, float center[], COLOR color);

    // variable declarations

    float radius = 0.02f;
    float center[2] = {-0.33f, 0.055f};
    // code
    // draw booton quad
    glBegin(GL_QUADS);
    glVertex3f((center[0] + 0.01) * SCALE + X_OFFSET, (center[1]) * SCALE + Y_OFFSET, 0.0f);
    glVertex3f((center[0] - 0.01) * SCALE + X_OFFSET, (center[1]) * SCALE + Y_OFFSET, 0.0f);
    glVertex3f((center[0] - 0.01) * SCALE + X_OFFSET, (center[1] - 0.035) * SCALE + Y_OFFSET, 0.0f);
    glVertex3f((center[0] + 0.01) * SCALE + X_OFFSET, (center[1] - 0.045) * SCALE + Y_OFFSET, 0.0f);
    glEnd();
    // draw outer circle
    drawCircle(radius, center, CERTIFICATES_COLORS);
    // draw inner circle
    radius = 0.007f;
    drawCircle(radius, center, WHITE);
}

void drawCircle(float radius, float center[], COLOR color)
{
    float PI = 22.0f / 7.0f;
    glBegin(GL_POLYGON);
    glColor3f(color.red, color.green, color.blue);
    for (float i = 0; i < 360; ++i)
    {
        float x = radius * sin(2 * PI * i / 360.0f);
        float y = radius * cos(2 * PI * i / 360.0f);
        glVertex3f((center[0] + x) * SCALE + X_OFFSET, (center[1] + y) * SCALE + Y_OFFSET, 0.0f);
    }
    glEnd();
}

#endif