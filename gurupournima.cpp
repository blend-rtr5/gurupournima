// header files
#include<GL/freeglut.h>
#include <math.h>
#include<iostream>

#include "computer.h"

#define PI 3.14159
int count = 0;

// globle variable declarations
bool bIsFullScreen = false;

// entry-point function
int main(int argc, char* argv[])
{
	//function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("RTR5 BLEND : Guru Pournima");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void) 
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	if (bIsFullScreen == false)
	{
		glutFullScreen();
		bIsFullScreen = true;
	}
	else
	{
		glutLeaveFullScreen();
		bIsFullScreen = false;
	}
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	void drawAyurvedDiagram(void);
	void drawTheatre(void);
	void drawKundali(void);

	// AMEY
	//Function prototypes	
	void centerSquare(void);
	void SquareTopRight(void);
	void SquareTopLeft(void);
	void SquareBottomRight(void);
	void SquareBottomLeft(void);
	void LineFrame(void);

	// AKSHAY
	void Ayurved(void);
	void Jyotish(void);
	void Natak(void);
	void Computer(void);
	void KnowledgeIsInterRelated(void);

	// code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();
	
	//count++;
	/*
	for (int i = 0; i < 10000000; i++ ) {
		for (int j = 1; j < 100; j++) {
			continue;
		}

		continue;
	}*/

	// AMEY
	centerSquare();

	if (count >= 1)
	{
		SquareBottomLeft();
		glColor3f(0.0f, 0.0f, 0.0f);
		Ayurved();
		drawAyurvedDiagram();
	}

	if (count >= 2)
	{
		SquareTopLeft();
		glColor3f(0.0f, 0.0f, 0.0f);
		Jyotish();

		drawKundali();
		glLoadIdentity();
	}

	if (count >= 3)
	{
		SquareTopRight();
		glColor3f(0.0f, 0.0f, 0.0f);
		Natak();
		drawTheatre();
	}

	if (count >= 4)
	{
		SquareBottomRight();
		glColor3f(0.0f, 0.0f, 0.0f);
		Computer();
		if (count == 4) {
			drawComputerLab();
		}
		else if (count > 5) {
			drawTheatre();
		}
	}

	if (count >= 5)
	{
		LineFrame();
	}

	if (count >= 6)
	{
		// SquareBottomRight();
		glColor3f(0.0f, 0.0f, 0.0f);
		KnowledgeIsInterRelated();
		count = 6;
	}

	
	glutSwapBuffers();
}

// AKSHAY
void Ayurved(void)
{
	glLoadIdentity();
	// For character position set on screen use this -->glRasterPos3f
	// void glRasterPos3f(GLfloat x,GLfloat y,GLfloat z);

	//glRasterPos3f(-1.0f, -0.8f, 0.0f);
	glRasterPos3f(-0.825f, -0.73f, 0.0f);
	// To set text color glcolor3f using currenly it is in white
	glColor3f(1.0f, 0.0f, 0.0f);
	// For rendering Fount - using glutBitmapCharacter
	// moreinfo - https://freeglut.sourceforge.net/docs/api.php#FontRendering
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'A');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'Y');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'U');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'R');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'V');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'E');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'D');

	glEnd();
}

// AKSHAY
void Jyotish(void)
{
	glLoadIdentity();
	//glRasterPos3f(-1.0f, 0.8f, 0.0f);
	glRasterPos3f(-0.80f, 0.715f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'J');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'Y');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'O');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'T');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'I');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'S');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'H');

	glEnd();
}

// AKSHAY
void Natak(void)
{
	glLoadIdentity();

	//glRasterPos3f(0.85f, 0.8f, 0.0f);
	glRasterPos3f(0.66f, 0.70f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'N');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'A');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'T');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'A');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'K');

	glEnd();
}

// AKSHAY
void Computer(void)
{
	glLoadIdentity();

	glRasterPos3f(0.62f, -0.75f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'C');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'O');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'M');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'P');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'U');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'T');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'E');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'R');
	glEnd();
}

// AKSHAY
void KnowledgeIsInterRelated(void)
{
	glLoadIdentity();

	glRasterPos3f(-0.3f, 0.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	// Knowledge is inter-related
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'K');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'N');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'O');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'W');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'L');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'E');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'D');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'G');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'E');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ' ');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'I');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'S');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ' ');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'I');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'N');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'T');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'E');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'R');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ' ');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'R');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'E');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'L');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'A');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'T');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'E');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'D');
	glEnd();
}

// AMEY
void centerSquare(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(0.85f, 0.815f, 0.76f);

	glBegin(GL_QUADS);

	glVertex3f(0.5, 0.5, 0.0);
	glVertex3f(-0.5, 0.5, 0.0);
	glVertex3f(-0.5, -0.5, 0.0);
	glVertex3f(0.5, -0.5, 0.0);

	glEnd();
}

// AMEY
void SquareTopRight(void)
{
	glColor3f(0.85f, 0.815f, 0.76f);

	glBegin(GL_QUADS);

	glVertex3f(0.85, 0.85, 0.0);
	glVertex3f(0.85, 0.6, 0.0);
	glVertex3f(0.6, 0.6, 0.0);
	glVertex3f(0.6, 0.85, 0.0);

	glEnd();
}

// AMEY
void SquareTopLeft(void)
{
	glColor3f(0.85f, 0.815f, 0.76f);

	glBegin(GL_QUADS);

	glVertex3f(-0.85, 0.85, 0.0);
	glVertex3f(-0.85, 0.6, 0.0);
	glVertex3f(-0.6, 0.6, 0.0);
	glVertex3f(-0.6, 0.85, 0.0);

	glEnd();
}

// AMEY
void SquareBottomRight(void)
{
	glColor3f(0.85f, 0.815f, 0.76f);

	glBegin(GL_QUADS);

	glVertex3f(0.85, -0.85, 0.0);
	glVertex3f(0.85, -0.6, 0.0);
	glVertex3f(0.6, -0.6, 0.0);
	glVertex3f(0.6, -0.85, 0.0);

	glEnd();
}

// AMEY
void SquareBottomLeft(void)
{
	glColor3f(0.85f, 0.815f, 0.76f);

	glBegin(GL_QUADS);

	glVertex3f(-0.85, -0.85, 0.0);
	glVertex3f(-0.85, -0.6, 0.0);
	glVertex3f(-0.6, -0.6, 0.0);
	glVertex3f(-0.6, -0.85, 0.0);

	glEnd();
}

// AMEY
void LineFrame(void)
{
	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_LINES);

	//Line on right side of origin
	glVertex3f(0.725, 0.6, 0.0);
	glVertex3f(0.725, -0.6, 0.0);

	//Line on left side of origin
	glVertex3f(-0.725, 0.6, 0.0);
	glVertex3f(-0.725, -0.6, 0.0);

	//Line on Bottom side of origin
	glVertex3f(0.6, -0.725, 0.0);
	glVertex3f(-0.6, -0.725, 0.0);

	//Line on Top side of origin
	glVertex3f(0.6, 0.725, 0.0);
	glVertex3f(-0.6, 0.725, 0.0);


	glEnd();
}


// JESWIN
GLfloat getRBG(int val)
{
	return ((GLfloat)val / 255);
}

// JESWIN
void Drawchar(GLfloat x, GLfloat y, char c)
{
	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glRasterPos2f(x, y);
	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)c);
}

// JESWIN
void drawKundali(void)//main 
{
	GLfloat getRBG(int val);
	void Drawchar(GLfloat x, GLfloat y, char c);

	glScalef(1.0f, 2.0f, 0.0f);
	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-0.5, -0.25);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.5, -0.25);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.5, 0.25);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(-0.5, 0.25);

	glEnd();

	//sets line thickness for all below
	glLineWidth(2.5);

	//bottom 
	//242, 184, 7

	glBegin(GL_LINES);
	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(-0.5, -0.25);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.5, -0.25);

	glEnd();

	//right
	glBegin(GL_LINES);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.5, -0.25);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.5, 0.25);

	glEnd();

	//top 
	glBegin(GL_LINES);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.5, 0.25);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(-0.5, 0.25);

	glEnd();

	//left
	glBegin(GL_LINES);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(-0.5, 0.25);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(-0.5, -0.25);

	glEnd();


	//rectangle done here

	//diagonal left to right
	glBegin(GL_LINES);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(-0.5, 0.25);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.5, -0.25);

	glEnd();

	//diagonal right to left
	glBegin(GL_LINES);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.5, 0.25);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(-0.5, -0.25);

	glEnd();


	//midpoint lines
	//left to top

	glBegin(GL_LINES);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(-0.5, 0.0);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.0, 0.25);

	glEnd();


	//left to bottom

	glBegin(GL_LINES);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(-0.5, 0.0);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.0, -0.25);

	glEnd();


	//right to top

	glBegin(GL_LINES);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.5, 0.0);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.0, 0.25);

	glEnd();

	//right to bottom

	glBegin(GL_LINES);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.5, 0.0);

	glColor3f(getRBG(242), getRBG(184), getRBG(7));
	glVertex2f(0.0, -0.25);

	glEnd();

	//2. writing text
	Drawchar(0.25f, 0.18f, '1');//
	Drawchar(0.27f, 0.18f, '2');//
	Drawchar(0.0f, 0.125f, '1');//
	Drawchar(-0.25f, 0.18f, '2');//
	Drawchar(-0.42f, 0.125f, '3');//
	Drawchar(-0.25f, 0.0f, '4');//
	Drawchar(-0.42f, -0.125f, '5');
	Drawchar(-0.25f, -0.21f, '6');//
	Drawchar(0.0f, -0.125f, '7');//
	Drawchar(0.25f, -0.2f, '8');//
	
	Drawchar(0.40f, -0.125f, '9');//10
	//Drawchar(0.42f, -0.125f, '1');//
	
	Drawchar(0.25f, 0.0f, '1');//11
	Drawchar(0.27f, 0.0f, '0');//
	
	Drawchar(0.40f, 0.125f, '1');//12
	Drawchar(0.42f, 0.125f, '1');//
}

// RENUKA
void drawTheatre(void)
{
	void drawCurtain_circle(double radius, double ori_x, double ori_y, int startpoint, int endpoint);
	glBegin(GL_QUADS);
	glColor3f(0.502, 0.0, 0);
	glVertex2f(-0.5, 0.5);
	glVertex2f(0.5, 0.5);
	glVertex2f(0.5, -0.5);
	glVertex2f(-0.5, -0.5);
	glEnd();

	// central white rect
	glBegin(GL_QUADS);
	glColor3f(1, 0.9529, 0.4275);
	glVertex2f(-0.4, 0.4);
	glVertex2f(0.4, 0.4);
	glVertex2f(0.4, -0.4);
	glVertex2f(-0.4, -0.4);
	glEnd();

	// blue rect
	glBegin(GL_QUADS);
	glColor3f(0.8, 0.85, 0.95);
	glVertex2f(-0.28, -0.1);	//2
	glVertex2f(0.28, -0.1);   //1
	glVertex2f(0.5, -0.5);  //4
	glVertex2f(-0.5, -0.5); //3
	glEnd();

	// left green rect
	glBegin(GL_QUADS);
	glColor3f(0.82, 0.62, 0.52);
	glVertex2f(-0.4, 0.4);
	glVertex2f(-0.4, -0.4);
	glVertex2f(-0.32, -0.4);
	glVertex2f(-0.32, 0.4);
	glEnd();

	// right green rect
	glBegin(GL_QUADS);
	glColor3f(0.82, 0.62, 0.52);
	glVertex2f(0.4, 0.4);
	glVertex2f(0.4, -0.4);
	glVertex2f(0.32, -0.4);
	glVertex2f(0.32, 0.4);
	glEnd();

	// left curtain downward squares
	glBegin(GL_QUADS);
	glColor3f(0.502, 0.0, 0);
	glVertex2f(-0.5, 0.5);
	glVertex2f(-0.45, 0.5);
	glVertex2f(-0.45, -0.5);
	glVertex2f(-0.5, -0.5);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.502, 0.0, 0);
	glVertex2f(-0.45, 0.5);
	glVertex2f(-0.4, 0.5);
	glVertex2f(-0.4, -0.48);
	glVertex2f(-0.45, -0.48);
	glEnd();

	// draw lines
	glLineWidth(1);
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_LINES);
	glVertex2f(-0.45, 0.5);
	glVertex2f(-0.45, -0.48);
	glEnd();

	// right curtain downward squares
	glBegin(GL_QUADS);
	glColor3f(0.502, 0.0, 0);
	glVertex2f(0.5, 0.5);
	glVertex2f(0.45, 0.5);
	glVertex2f(0.45, -0.5);
	glVertex2f(0.5, -0.5);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.502, 0.0, 0);
	glVertex2f(0.45, 0.5);
	glVertex2f(0.4, 0.5);
	glVertex2f(0.4, -0.48);
	glVertex2f(0.45, -0.48);
	glEnd();

	// draw lines
	glLineWidth(1);
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_LINES);
	glVertex2f(0.45, 0.5);
	glVertex2f(0.45, -0.48);
	glEnd();

	// draw curtain circles
	glColor3f(0.502, 0.0, 0);;
	drawCurtain_circle(0.15, 0.0, 0.5, 150, 300);
	drawCurtain_circle(0.15, 0.15, 0.5, 150, 300);
	drawCurtain_circle(0.15, 0.35, 0.5, 150, 300);
	drawCurtain_circle(0.15, -0.15, 0.5, 150, 300);
	drawCurtain_circle(0.15, -0.35, 0.5, 150, 300);

}

// RENUKA
// Funtion Used To Draw Cake Circles
void drawCurtain_circle(double radius, double ori_x, double ori_y, int startpoint, int endpoint) // Function Definition
{
	glBegin(GL_POLYGON);
	for (int i = startpoint; i <= endpoint; i++)
	{
		double angle = 2 * PI * i / 300;
		double x = cos(angle) * radius;
		double y = sin(angle) * radius;
		glVertex3f(ori_x + x, ori_y + y, 0.0);
	}
	glEnd();
}

// PRATISHA
// Draw Ayurved Diagram
void drawAyurvedDiagram(void)
{
	// Ayurved Diagram Function declarations
	void drawAyurved_rectangle(float, float, float, float);
	void drawAyurved_circle(double, double, double, int, int);
	void drawAyurved_triangle(float, float, float, float, float, float);
	void drawAyurved_line(float, float, float, float, float, float);
	void drawAyurved_ploygon(float, float, float, float, float, float, float, float);
	void drawAyurved_text(float, float, char*);

	// Syurvel Diagram Function Calls
	glColor3f(1.0f, 1.0f, 1.0f);
	drawAyurved_rectangle(0.51f, 0.51f, -0.51f, -0.51f);
	glColor3f(0.0f, 0.0f, 0.0f);
	drawAyurved_circle(0.5f, 0.0f, 0.0f, 0, 300);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawAyurved_circle(0.495f, 0.0f, 0.0f, 0, 300);

	glColor3f(0.0f, 0.0f, 0.0f);
	drawAyurved_line(0.0f, 0.5f, -0.15f, 0.15f, 0.15f, 0.15f);
	drawAyurved_line(-0.48f, 0.15f, -0.15f, 0.15f, -0.2f, -0.05f);
	drawAyurved_line(-0.3f, -0.4f, -0.2f, -0.05f, 0.0f, -0.15f);
	drawAyurved_line(0.48f, 0.15f, 0.15f, 0.15f, 0.2f, -0.05f);
	drawAyurved_line(0.3f, -0.4f, 0.2f, -0.05f, 0.0f, -0.15f);

	glColor3f(1.0f, 0.0f, 1.0f);
	drawAyurved_triangle(0.0f, 0.5f, -0.15f, 0.15f, 0.15f, 0.15f);
	glColor3f(1.0f, 0.5f, 0.0f);
	drawAyurved_triangle(-0.48f, 0.15f, -0.15f, 0.15f, -0.2f, -0.05f);
	glColor3f(1.0f, 1.0f, 0.0f);
	drawAyurved_triangle(-0.3f, -0.4f, -0.2f, -0.05f, 0.0f, -0.15f);
	glColor3f(2.0f, 0.5f, 0.2f);
	drawAyurved_triangle(0.48f, 0.15f, 0.15f, 0.15f, 0.2f, -0.05f);
	glColor3f(0.0f, 0.5f, 1.0f);
	drawAyurved_triangle(0.3f, -0.4f, 0.2f, -0.05f, 0.0f, -0.15f);

	glColor3f(0.0f, 1.0f, 0.0f);
	drawAyurved_ploygon(-0.15f, 0.15f, 0.15f, 0.15f, 0.0f, 0.0f, -0.2f, -0.05f);
	glColor3f(0.0f, 0.0f, 1.0f);
	drawAyurved_ploygon(0.15f, 0.15f, 0.2f, -0.05f, 0.1f, -0.1f, 0.0f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	drawAyurved_ploygon(-0.2f, -0.05f, 0.0f, 0.0f, 0.1f, -0.1f, 0.0f, -0.15f);

	glColor3f(0.0f, 0.0f, 0.0f);
	drawAyurved_triangle(0.0f, 0.5f, 0.02f, 0.52f, 0.02f, 0.48f);
	drawAyurved_triangle(-0.48f, 0.15f, -0.485f, 0.17f, -0.45f, 0.17f);
	drawAyurved_triangle(-0.3f, -0.4f, -0.3f, -0.38f, -0.34f, -0.4f);
	drawAyurved_triangle(0.48f, 0.15f, 0.46f, 0.13f, 0.5f, 0.13f);
	drawAyurved_triangle(0.3f, -0.4f, 0.28f, -0.38f, 0.28f, -0.43f);

	drawAyurved_triangle(0.0f, 0.5f, -0.02f, 0.52f, -0.02f, 0.48f);
	drawAyurved_triangle(-0.48f, 0.15f, -0.5f, 0.13f, -0.46f, 0.13f);
	drawAyurved_triangle(-0.3f, -0.4f, -0.27f, -0.4f, -0.27f, -0.44f);
	drawAyurved_triangle(0.48f, 0.15f, 0.45f, 0.17f, 0.49f, 0.17f);
	drawAyurved_triangle(0.3f, -0.4f, 0.32f, -0.36f, 0.32f, -0.41f);

	glColor3f(0.0, 0.0, 0.0);
	drawAyurved_text(-0.03f, 0.28f, "ether");
	drawAyurved_text(-0.3f, 0.07f, "air");
	drawAyurved_text(-0.18f, -0.21f, "fire");
	drawAyurved_text(0.21f, 0.07f, "earth");
	drawAyurved_text(0.11f, -0.19f, "water");

	drawAyurved_text(-0.12f, 0.04f, "vatha");
	drawAyurved_text(-0.05f, -0.08f, "pitha");
	drawAyurved_text(0.07f, -0.02f, "kaptha");

	drawAyurved_text(-0.3f, 0.3f, "movement");
	drawAyurved_text(-0.39f, -0.16f, "light");
	drawAyurved_text(0.2f, 0.3f, "cold");
	drawAyurved_text(0.3f, -0.13f, "cohesion");
	drawAyurved_text(-0.1f, -0.34f, "transformation");
}

// PRATISHA
// Used To Draw Polygons For Ayurved Diagram
void drawAyurved_ploygon(float x, float y, float x1, float y1, float x2, float y2, float x3, float y3) // Function Definition Of Ayurved Diagram Polygon Function
{
	glBegin(GL_POLYGON);

	glVertex3f(x, y, 0.0f);
	glVertex3f(x1, y1, 0.0f);
	glVertex3f(x2, y2, 0.0f);
	glVertex3f(x3, y3, 0.0f);

	glEnd();
}

// PRATISHA
// Used To Draw Texts For Ayurved Diagram
void drawAyurved_text(float x, float y, char* str) // Function Definition Of Ayurved Diagram Text Function
{
	glRasterPos2f(x, y); //define position on the screen
	char* string = str;

	while (*string) {
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *string++);
	}
}

// PRATISHA
// Used To Draw Triangles For Ayurved Diagram
void drawAyurved_triangle(float x, float y, float x1, float y1, float x2, float y2) // Function Definition Of Ayurved Diagram Triangle Function
{
	glBegin(GL_TRIANGLES);

	glVertex3f(x, y, 0.0f);
	glVertex3f(x1, y1, 0.0f);
	glVertex3f(x2, y2, 0.0f);

	glEnd();
}

// PRATISHA
// Used To Draw Lines For Ayurved Diagram
void drawAyurved_line(float x, float y, float x1, float y1, float x2, float y2) // Function Definition Of Ayurved Diagram Line Function
{
	glBegin(GL_LINE_LOOP);

	glVertex3f(x, y, 0.0f);
	glVertex3f(x1, y1, 0.0f);
	glVertex3f(x2, y2, 0.0f);

	glEnd();
}

// PRATISHA
// Used To Draw Rectangles For Ayurved Diagram
void drawAyurved_rectangle(float x, float y, float x1, float y1)// Function Definition Of Ayurved Diagram Rectangle Function
{
	glBegin(GL_QUADS);

	glVertex3f(x, y, 0.0f);
	glVertex3f(x1, y, 0.0f);
	glVertex3f(x1, y1, 0.0f);
	glVertex3f(x, y1, 0.0f);

	glEnd();
}

// PRATISHA
// Used To Draw Circles For Ayurved Diagram
void drawAyurved_circle(double radius, double ori_x, double ori_y, int startpoint, int endpoint) // Function Definition Of Ayurved Diagram Circle Function
{
	glBegin(GL_POLYGON);
	for (int i = startpoint; i <= endpoint; i++)
	{
		double angle = 2 * PI * i / 300;
		double x = cos(angle) * radius;
		double y = sin(angle) * radius;
		glVertex3f(ori_x + x, ori_y + y, 0.0);
	}
	glEnd();
}


void keyboard(unsigned char key, int x, int y) 
{
	// code 
	switch (key) 
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if(bIsFullScreen==false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	case 'C':
	case 'c':
		count++;
		display();
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code

}